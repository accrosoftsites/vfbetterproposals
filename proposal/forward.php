<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width, initial-scale =1.0, user-scalable =no">

<link href='https://betterproposals.io/proposal/custom.css' rel='stylesheet' type='text/css'>

<?php 
    $url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

    if (strpos(strtolower($url),'weduc') !== false) {
        echo "<title>Proposal | Weduc</title>";
        echo "<link rel='shortcut icon' href='https://accropress.co.uk/app/uploads/sites/2/2018/02/cropped-Weduc_W_icon_512px-32x32.png'>";
    } else {
        echo "<title>Proposal | Vacancy Filler</title>";
        echo "<link rel='shortcut icon' href='https://accropress.co.uk/app/uploads/sites/4/2017/10/favicon-1.png'>";
    }
?>
</head>

<body>

<iframe id="iframe1" frameborder="0" src="https://betterproposals.io/proposal/forward.php?ProposalID=<?php echo $_GET['ProposalID'];?>&ContactID=<?php echo $_GET['ContactID'];?>"></iframe> 

</body>
</html>
