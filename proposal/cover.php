<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta name="robots" content="noindex, nofollow">
		<meta name="viewport" content="width=device-width, initial-scale =1.0, user-scalable =no">
		<?php 
			$url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

			if (strpos(strtolower($url),'weduc') !== false) {
				echo "<link rel='stylesheet' href='css/WeducEffect.css' type='text/css' />";
				echo "<title>Proposal | Weduc</title>";
				echo "<link rel='shortcut icon' href='https://accropress.co.uk/app/uploads/sites/2/2018/02/cropped-Weduc_W_icon_512px-32x32.png'>";
			} else {
				echo "<link rel='stylesheet' href='css/VFEffect.css' type='text/css' />";
				echo "<title>Proposal | Vacancy Filler</title>";
				echo "<link rel='shortcut icon' href='https://accropress.co.uk/app/uploads/sites/4/2017/10/favicon-1.png'>";
			}
		?>
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link href='https://betterproposals.io/proposal/custom.css' rel='stylesheet' type='text/css'>	
		<script src="js/modernizr.custom.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	</head>
	<body class="demo-2"></body>
		<div id="ip-container" class="ip-container">
			<header class="ip-header">
				<h1 class="ip-logo">
					<?php 
						$url = 'https://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

						if (strpos(strtolower($url),'weduc') !== false) {
							echo "<img class='ip-inner' src='css/WeducLogo.png' />";
						} else {
							echo "<img class='ip-inner' src='css/VFLogo.png' />";
						}
					?>
				</h1>
				<div class="ip-loader">
					<svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
						<path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
						<path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
					</svg>
				</div>
			</header>
			<div class="ip-main">
				<?php if (isset($_GET['ContactID'])) { ?>
				<iframe id="iframe1" frameborder="0" src="https://betterproposals.io/proposal/cover.php?ProposalID=<?php echo $_GET['ProposalID'];?>&ContactID=<?php echo $_GET['ContactID'];?>"></iframe> 
				<?php } else {?>
				<iframe id="iframe1" frameborder="0" src="https://betterproposals.io/proposal/cover.php?ProposalID=<?php echo $_GET['ProposalID'];?>&debug=yes"></iframe> 
				<?php } ?>
			</div>
		</div>
	<script src="js/classie.js"></script>
	<script src="js/pathLoader.js"></script>
	<script src="js/main.js"></script>
</html>
