<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<meta name="robots" content="noindex, nofollow">
<meta name="viewport" content="width=device-width, initial-scale =1.0, user-scalable =no">

<link href='https://betterproposals.io/proposal/custom.css' rel='stylesheet' type='text/css'>

<?php
			$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

			//var_dump($_SERVER);

			//echo "url - " . $url . "<br>";

			if (strpos(strtolower($url),'weduc') !== false) {
			    //echo "weduc<br>";
				echo "<link rel='stylesheet' href='css/WeducEffect.css' type='text/css' />";
				echo "<title>Proposal | Weduc</title>";
				echo "<link rel='shortcut icon' href='https://accropress.co.uk/app/uploads/sites/2/2018/02/cropped-Weduc_W_icon_512px-32x32.png'>";
			} else {
			    //echo "not weduc<br>";
				echo "<link rel='stylesheet' href='css/VFEffect.css' type='text/css' />";
				echo "<title>Proposal | Vacancy Filler</title>";
				echo "<link rel='shortcut icon' href='https://accropress.co.uk/app/uploads/sites/4/2017/10/favicon-1.png'>";
			}
		?>
</head>

<body>

<?php if (isset($_GET['ContactID'])) {?>
<iframe id="iframe1" frameborder="0" src="https://betterproposals.io/proposal/index.php?ProposalID=<?php echo $_GET['ProposalID'];?>&ContactID=<?php echo $_GET['ContactID'];?>" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
<?php } else { ?>
<iframe id="iframe1" frameborder="0" src="https://betterproposals.io/proposal/index.php?ProposalID=<?php echo $_GET['ProposalID'];?>&debug=yes" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> 
<?php } ?>

</body>
</html>
